package org.service;

import org.junit.jupiter.api.Test;
import org.service.converter.dto.DepartmentDto;
import org.service.converter.dto.PersonDto;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MapperTest {

    @Test
    public void testMapping(){
        String testName = "Name";
        String testSurname = "Surname";
        String departmentName = "Department name";

        PersonDto person = mock(PersonDto.class);
        when(person.getName()).thenReturn(testName);
        when(person.getSurname()).thenReturn(testSurname);

        DepartmentDto department = mock(DepartmentDto.class);
        when(department.getName()).thenReturn(departmentName);

        Result result = Mapper.mapDtosToResult(person, department);

        assertEquals(testName, result.getPersonName());
        assertEquals(testSurname, result.getPersonSurname());
        assertEquals(departmentName, result.getDepartmentName());

    }

}
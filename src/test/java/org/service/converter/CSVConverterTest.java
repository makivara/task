package org.service.converter;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.service.converter.dto.DepartmentDto;
import org.service.converter.dto.PersonDto;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CSVConverterTest {

    private CSVConverter<PersonDto> converter = new CSVConverter<>(PersonDto.class);
    private CSVConverter<DepartmentDto> departmentConverter = new CSVConverter<>(DepartmentDto.class);

    @TempDir
    File tempFolder;

    @Test
    void convertPersonCsvToDto() throws IOException {
        File persons = new File(tempFolder, "persons.csv");
        List<String> lines = Arrays.asList("id;name;surname;department_id", "1;John;Smith;2", "2;Pete;Hallock;1");
        Files.write(persons.toPath(), lines);

        List<PersonDto> result = converter.convertCsvToDto(persons);

        assertAll(
                () -> assertEquals(2, result.size()),
//                first Person
                () -> assertEquals(1, result.get(0).getId()),
                () -> assertEquals("John", result.get(0).getName()),
                () -> assertEquals("Smith", result.get(0).getSurname()),
                () -> assertEquals(2, result.get(0).getDepartmentId()),
//                second Person
                () -> assertEquals(2, result.get(1).getId()),
                () -> assertEquals("Pete", result.get(1).getName()),
                () -> assertEquals("Hallock", result.get(1).getSurname()),
                () -> assertEquals(1, result.get(1).getDepartmentId())
        );

    }

    @Test
    void convertPersonCsvWithOnlyHeadersShouldReturnEmptyList() throws IOException {
        File persons = new File(tempFolder, "persons.csv");
        List<String> lines = Arrays.asList("id;name;surname;department_id");
        Files.write(persons.toPath(), lines);

        List<PersonDto> result = converter.convertCsvToDto(persons);

        assertAll(
                () -> assertEquals(0, result.size())
        );

    }

    @Test
    void convertEmptyCsvShouldReturnEmptyList() throws IOException {
        File persons = new File(tempFolder, "persons.csv");
        List<String> lines = Collections.emptyList();
        Files.write(persons.toPath(), lines);

        List<PersonDto> result = converter.convertCsvToDto(persons);

        assertAll(
                () -> assertEquals(0, result.size())
        );
    }

    @Test
    void convertDepartmentCsvToDto() throws IOException {
        File departments = new File(tempFolder, "departments.csv");
        List<String> lines = Arrays.asList("id;name", "1;Marketing", "2;Finance");
        Files.write(departments.toPath(), lines);

        List<DepartmentDto> result = departmentConverter.convertCsvToDto(departments);

        assertAll(
                () -> assertEquals(2, result.size()),
//                first
                () -> assertEquals(1, result.get(0).getId()),
                () -> assertEquals("Marketing", result.get(0).getName()),
//                second
                () -> assertEquals(2, result.get(1).getId()),
                () -> assertEquals("Finance", result.get(1).getName())
        );
    }
}
package org;

import org.service.DtoMergeHandler;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class Application {

    public static void main(String[] args) throws URISyntaxException {
        Application app = new Application();
        File personDataFile = app.getFileFromResource("Test Task.csv");
        File departmentDataFile = app.getFileFromResource("Test_Task.csv");

        DtoMergeHandler handler = new DtoMergeHandler();
        handler.mergePersonAndDepartmens(personDataFile, departmentDataFile).forEach(System.out::println);
    }

    private File getFileFromResource(String fileName) throws URISyntaxException {

        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file not found! " + fileName);
        } else {
            return new File(resource.toURI());
        }
    }
}

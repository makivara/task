package org.service;

import org.service.converter.CSVConverter;
import org.service.converter.dto.DepartmentDto;
import org.service.converter.dto.PersonDto;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class DtoMergeHandler {

    Map<Class, CSVConverter> converterMap;

    public DtoMergeHandler() {
        configureConverters();
    }

    public List<Result> mergePersonAndDepartmens(File personFile, File departmentFile) {

        try {
            List<PersonDto> personDtos = converterMap.get(PersonDto.class).convertCsvToDto(personFile);
            Map<Long, List<PersonDto>> personsMap = personDtos.stream()
                    .filter(Objects::nonNull)
                    .collect(Collectors.groupingBy(PersonDto::getDepartmentId));
            List<DepartmentDto> departmentDtos = converterMap.get(DepartmentDto.class).convertCsvToDto(departmentFile);
            return departmentDtos.stream()
                    .filter(Objects::nonNull)
                    .flatMap(departmentDto -> this.merge(departmentDto, personsMap.get(departmentDto.getId())).stream())
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException("Could not read data from file", e);
        }
    }

    private List<Result> merge(DepartmentDto department, List<PersonDto> personDtoList) {
        if(Objects.isNull(personDtoList)) {
            return Collections.emptyList();
        }
        return personDtoList.stream()
                .map(person -> Mapper.mapDtosToResult(person, department))
                .collect(Collectors.toList());
    }

    private void configureConverters() {
        Map<Class, CSVConverter> converters = new HashMap<>();
        converters.put(PersonDto.class, new CSVConverter(PersonDto.class));
        converters.put(DepartmentDto.class, new CSVConverter(DepartmentDto.class));
        converterMap = Collections.unmodifiableMap(converters);
    }


}

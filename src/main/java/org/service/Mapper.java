package org.service;

import org.service.converter.dto.DepartmentDto;
import org.service.converter.dto.PersonDto;

public class Mapper {

    public static Result mapDtosToResult(PersonDto person, DepartmentDto department){
        Result result = new Result();
        result.setPersonName(person.getName());
        result.setPersonSurname(person.getSurname());
        result.setDepartmentName(department.getName());
        return result;
    }
}

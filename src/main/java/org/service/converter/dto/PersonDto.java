package org.service.converter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "id",
        "name",
        "surname",
        "department_id"
})
public class PersonDto extends AbstractDto {

    private String surname;
    @JsonProperty("department_id")
    private Long departmentId;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }


    @Override
    public String toString() {
        return "PersonDto{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", surname='" + surname + '\'' +
                ", departmentId=" + departmentId +
                '}';
    }
}

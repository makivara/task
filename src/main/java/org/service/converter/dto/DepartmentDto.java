package org.service.converter.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "id",
        "name"
})
public class DepartmentDto extends AbstractDto {

    @Override
    public String toString() {
        return "DepartmentDto{" +
                "id=" + this.getId() +
                ", name='" + this.getName() + '\'' +
                '}';
    }
}

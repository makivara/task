package org.service.converter;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.service.converter.dto.AbstractDto;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class CSVConverter<T> {

    private CsvMapper mapper = new CsvMapper();
    private Class<T> type;

    public CSVConverter(Class<T> type){
        this.type = type;
    }

    public <T extends AbstractDto> List<T> convertCsvToDto(File file) throws IOException {

        CsvSchema schema = mapper.schemaFor(this.type).withHeader().withColumnSeparator(';');

        MappingIterator<T> it = mapper.disable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY)
                .readerFor(this.type)
                .with(schema)
                .readValues(file);
        return it.readAll();
    }
}
